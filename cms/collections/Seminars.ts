import type { CollectionConfig } from 'payload/types';

const Seminars: CollectionConfig = {
  slug: 'seminars',
  admin: {
    useAsTitle: 'title'
  },
  access: {
    create: () => true,
    read: () => true
  },
  fields: [
    {
        name: 'title',
        type: 'text',
    },
    {
        name: 'description',
        type: 'text',
    },
    {
        name: 'price',
        type: 'number'
    },
    {
      name:'maximum_participants',
      type:'number'
    },
    {
      name:'begin_date',
      type:'date'
    },
    {
      name:'end_date',
      type:'date'
    },
    {
      name:'time_in_hours',
      type:'number'
    },
    {
      name:'seminar_type',
      type:'text'
    },
    {
      name:'seminar_manager',
      type:'relationship',
      relationTo: 'users'
    }
    ],
   /*  upload: {
      staticDir:'media',
      mimeTypes: ['image/png', 'image/jpeg', 'application/pdf']
      
      }, */
};

export default Seminars;
