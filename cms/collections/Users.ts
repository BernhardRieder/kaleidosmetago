import type { CollectionConfig } from "payload/types";
import { admins } from "./access/admins";
import adminsAndUser from "./access/adminsAndUser";
import { checkRole } from "./access/checkRole";
import { protectRoles } from "./hooks/protectRoles";

const Users: CollectionConfig = {
  slug: "users",
  auth: {},
  admin: {
    useAsTitle: "email",
  },
  access: {
    // Anyone can create a user
    create: () => true,

    // Users with role 'admin' can read and update all users
    // But users with role 'customer' can only read and update their own
    read: adminsAndUser,
    update: adminsAndUser,

    // Only admins can delete
    delete: admins,
    admin: ({ req: { user } }) => checkRole(["admin"], user),
  },
  fields: [
    // Email added by default
    // Add more fields as needed
    {
      name: "phone",
      type: "text",
      required: true,
    },
    {
      name: "firstName",
      type: "text",
      required: true,
      saveToJWT: true,
    },
    {
      name: "lastName",
      type: "text",
      required: true,
      saveToJWT: true,
    },
    {
      name: "roles",
      type: "select",
      hasMany: true,
      required: true,
      saveToJWT: true,
      hooks: {
        beforeChange: [protectRoles],
      },
      options: [
        {
          label: "Admin",
          value: "admin",
        },
        {
          label: "Betreuer",
          value: "betreuer",
        },
        {
          label: "Customer",
          value: "customer",
        },
      ],
    },
  ],
};

export default Users;
