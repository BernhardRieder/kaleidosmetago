#Kaleidosmetago



Kaleidosmetago ist ein Diplomprojekt, dessen Ziel es ist, eine Web-App zum Kauf und zum Anbieten von Seminaren zu erstellen. Benutzer sollen anstehende Seminare kaufen können und autorisierte Benutzer können Seminare erstellen und freigeben.

Das gewählte Kommunikationsmittel ist Discord.

Das Use-Case Diagramm

![Use-Case Diagramm](/documentation/Bernhard_Rieder_Kaleidosmetago_UML_Diagramm.svg)

gebaut auf <https://github.com/manawiki/repay>

Simple template to get started with [Remix](https://remix.run) and [PayloadCMS](https://payloadcms.com), without the monorepo messiness!

![repay-header](https://github.com/manawiki/repay/assets/84349818/9fc343c2-0c6f-4d2d-a603-c838f8d21156)

## Development

**wir benutzen `yarn`**

Start the mongoDB
`docker compose -f .\docker-compose-mongo.yml up`

Copy .env.example to .env and fill the required environment variables.

```sh
yarn;
yarn dev
```

## Links

[Dokumentation und Projektantrag](https://htlimst.sharepoint.com/sites/Diplompojekt_Kaleidosmetago/Freigegebene%20Dokumente/Forms/AllItems.aspx)


[Figma design](https://www.figma.com/design/pkAV8Wwqy8hl6HO8NstBYR/Kaleidosmetago-Design?node-id=29-781&t=Z1GHa04HXGVSUqLj-1)

## Deployment

First, build your app for production:

```sh
yarn build
```

Then run the app in production mode:

```sh
yarn start
```