import type { ActionFunctionArgs, LoaderFunctionArgs } from "@remix-run/node";
import { json, redirect } from "@remix-run/node";

import { Form, Link, useActionData } from "@remix-run/react";

// import { PayloadRequest } from "payload/types";

export const loader = async ({
  context: { payload, user },
}: LoaderFunctionArgs) => {
  if (user) {
    return redirect("/");
  }

  return json({}, { status: 200 });
};

export async function action({
  request,
  context: { payload, res, req },
}: ActionFunctionArgs) {
  const formData = await request.formData();
  const email = String(formData.get("email"));
  const firstName = String(formData.get("firstName"));
  const lastName = String(formData.get("lastName"));
  const phone = String(formData.get("phone"));
  const password = String(formData.get("password"));
  const passwordRepeat = String(formData.get("passwordRepeat"));

  let errors: Record<string, string[]> = {};

  if (!email.match(/@/)) {
    errors.email = ["Email is not valid"];
  }
  if (!firstName || firstName.length < 2) {
    errors.firstName = [
      "First name is required and must be at least 2 characters",
    ];
  }
  if (!lastName || lastName.length < 2) {
    errors.lastName = [
      "Last name is required and must be at least 2 characters",
    ];
  }
  if (!phone) {
    errors.phone = ["Phone number is not valid"];
  }

  if (password !== passwordRepeat) {
    errors.passwordRepeat = ["Passwords do not match"];
    errors.password = ["Passwords do not match"];
  }
  if (password.length < 8) {
    errors.password = [
      ...(errors.password ?? []),
      "Password must be at least 8 characters",
    ];
  }

  if (Object.keys(errors).length > 0) {
    return json({ errors }, { status: 400 });
  }

  // Validate credentials

  const result = await payload.create({
    collection: "users", // required
    data: {
      email,
      password,
      firstName,
      lastName,
      phone,
      roles: ["customer"],
      // roles: ["admin", "customer"],
    },
    req, // pass a Request object to be provided to all hooks
  });

  console.log("result", result, email, password);

  const loginResult = await payload.login({
    collection: "users", // required
    data: {
      email,
      password,
    },
    req, // pass a Request object to be provided to all hooks
    res: res, // used to automatically set an HTTP-only auth cookie
  });

  console.log("loginResult", loginResult, email, password);

  // Redirect to dashboard if validation is successful
  // return res.json(result.user);
  return null;
}

const FormField = ({
  label,
  id,
  type,
  placeholder,
  error: errors,
}: {
  label: string;
  id: string;
  type: string;
  placeholder: string;
  error?: string[];
}) => (
  <div className="flex flex-col justify-start items-start self-stretch flex-grow-0 flex-shrink-0 relative gap-2">
    <label
      htmlFor={id}
      className="self-stretch flex-grow-0 flex-shrink-0 w-[329px] sm:w-80 text-base text-left text-[#1e1e1e]"
    >
      {label}
    </label>
    <div
      className={`flex justify-start items-center self-stretch flex-grow-0 flex-shrink-0 relative overflow-hidden px-4 py-3 rounded-lg bg-white border ${
        errors ? "border-red-500" : "border-[#d9d9d9]"
      }`}
    >
      <input
        type={type}
        name={id}
        id={id}
        placeholder={placeholder}
        required
        className="flex-grow w-auto text-base text-left placeholder:text-[#b3b3b3] outline-none"
        aria-invalid={errors ? "true" : "false"}
        aria-describedby={errors ? `${id}-error` : undefined}
      />
    </div>
    {errors && (
      <span id={`${id}-error`} className="text-sm text-red-500">
        {errors.join(", ")}
      </span>
    )}
  </div>
);

export default function Register() {
  const actionData = useActionData<typeof action>();

  // Extract field-specific errors from the response
  const getFieldError = (fieldName: string): string[] | undefined => {
    if (!actionData?.errors) return undefined;
    if (actionData.errors[fieldName]) {
      return actionData.errors[fieldName];
    }
    return undefined;
  };

  return (
    <div className="flex flex-col items-center justify-center h-full">
      <Form
        action="/register"
        method="post"
        className="flex flex-col justify-start items-start flex-grow-0 flex-shrink-0 gap-6 p-6 sm:rounded-lg sm:bg-white sm:border sm:border-[#d9d9d9] m-8"
      >
        <div className="hidden justify-start items-center flex-grow-0 flex-shrink-0 relative overflow-hidden gap-6 py-1 sm:flex">
          <img
            src="/public/favicon.png"
            className="flex-grow-0 flex-shrink-0 w-20 h-20 object-cover"
            alt="Logo"
          />
          <p className="flex-grow-0 flex-shrink-0 w-56 h-12 text-2xl font-extralight text-left text-[#6a6a6a]">
            KALEIDOSMETAGO
          </p>
        </div>

        <FormField
          label="Vorname"
          id="firstName"
          type="text"
          placeholder="Vorname"
          error={getFieldError("firstName")}
        />

        <FormField
          label="Nachname"
          id="lastName"
          type="text"
          placeholder="Nachname"
          error={getFieldError("lastName")}
        />

        <FormField
          label="Telefon"
          id="phone"
          type="tel"
          placeholder="Telefon"
          error={getFieldError("phone")}
        />

        <FormField
          label="Email"
          id="email"
          type="email"
          placeholder="Email"
          error={getFieldError("email")}
        />

        <FormField
          label="Passwort"
          id="password"
          type="password"
          placeholder="Passwort"
          error={getFieldError("password")}
        />

        <FormField
          label="Passwort wiederholen"
          id="passwordRepeat"
          type="password"
          placeholder="Passwort wiederholen"
          error={getFieldError("passwordRepeat")}
        />

        <div className="flex justify-start items-center self-stretch flex-grow-0 flex-shrink-0 h-10 gap-4">
          <button
            type="submit"
            className="flex justify-center items-center flex-grow relative overflow-hidden gap-2 p-3 rounded-lg bg-[#2c2c2c] hover:bg-[#404040] transition-colors border border-[#2c2c2c]"
          >
            <span className="flex-grow-0 flex-shrink-0 text-base text-left text-neutral-100">
              Registrieren
            </span>
          </button>
        </div>

        <div className="flex justify-center items-center self-stretch flex-grow-0 flex-shrink-0 relative overflow-hidden">
          <Link
            to="/login"
            className="flex-grow-0 flex-shrink-0 text-base text-left text-black hover:underline"
          >
            Anmelden
          </Link>
        </div>
      </Form>
    </div>
  );
}
