import type { ActionFunctionArgs, LoaderFunctionArgs } from "@remix-run/node";
import {
  Form,
  useActionData,
  useLoaderData,
  useRouteError,
} from "@remix-run/react";
import { json, redirect } from "@remix-run/node";

export const loader = async ({ context: { payload } }: LoaderFunctionArgs) => {
  const users = await payload.find({
    collection: "users",
  });
  const seminars = await payload.find({
    collection: "seminars",
  });

  return json(
    { userCount: users.totalDocs, allSeminars: seminars },
    { status: 200 }
  );
};

export async function action({
  request,
  context: { payload, user },
}: ActionFunctionArgs) {

  const body = await request.formData();

  const title = String(body.get("title"));
  const description = String(body.get("description"));
  const price = Number(body.get("price"));
  const maximum_participants = Number(body.get("maximum_participants"));
  const begin_date = String(body.get("begin_date"));
  const end_date = String(body.get("end_date"));
  const time_in_hours = Number(body.get("time_in_hours"));
  const seminar_type = String(body.get("seminar_type"));

  const errors: { [key: string]: string } = {};

  const titleBounds = [2, 250];
  const descriptionBounds = [10, 1000];

  console.log(user);

  if (title != null && title != undefined && title != "") {
    if (title.length < titleBounds[0] || title.length > titleBounds[1]) {
      errors.title =
        "Titel muss zwischen " +
        titleBounds[0] +
        " und " +
        titleBounds[1] +
        " Zeichen lang sein!";
    }
  } else {
    errors.title = "Titel darf nicht leer sein!";
  }

  if (description != null && description != undefined && description != "") {
    if (
      description.length < descriptionBounds[0] ||
      description.length > descriptionBounds[1]
    ) {
      errors.description =
        "Beschreibung muss zwischen " +
        descriptionBounds[0] +
        " und " +
        descriptionBounds[1] +
        " Zeichen lang sein!";
    }
  } else {
    errors.description = "Beschreibung darf nicht leer sein!";
  }

  if (price != null || price != undefined) {
    if (price <= 0) {
      errors.price = "Preis muss größer als 0 sein!";
    }
  } else {
    errors.price = "Preis darf nicht leer sein!";
  }

  if (maximum_participants != null || maximum_participants != undefined) {
    if (maximum_participants <= 0) {
      errors.maximum_participants =
        "Maximale Teilnehmeranzahl muss größer als 0 sein!";
    }
  } else {
    errors.maximum_participants =
      "Maximale Teilnehmeranzahl darf nicht leer sein!";
  }

  if (begin_date != null || begin_date != undefined || begin_date != "") {
    if (begin_date > end_date) {
      errors.begin_date = "Beginndatum darf nicht größer als Enddatum sein!";
    }
  } else {
    errors.begin_date = "Beginndatum darf nicht leer sein!";
  }

  if (end_date == null || end_date == undefined || end_date == "") {
    errors.end_date = "Enddatum darf nicht leer sein!";
  }

  if (time_in_hours != null || time_in_hours != undefined) {
    if (time_in_hours <= 0) {
      errors.time_in_hours = "Zeitdauer pro Tag muss größer als 0 sein!";
    }
  } else {
    errors.time_in_hours = "Zeitdauer pro Tag darf nicht leer sein!";
  }

  if (seminar_type != null || seminar_type != undefined || seminar_type != "") {
    if (
      seminar_type != "present" &&
      seminar_type != "online" &&
      seminar_type != "hybrid"
    ) {
      errors.seminar_type =
        "Seminartyp muss 'Präsenz', 'Online' oder 'Hybrid' sein!";
    }
  } else {
    errors.seminar_type = "Seminartyp darf nicht leer sein!";
  }

  if (Object.keys(errors).length > 0) {
    console.log(errors);
    return json({ errors });
  }

  const result = await payload.create({
    collection: "seminars",
    data: {
      title: title,
      description: description,
      price: price,
      maximum_participants: maximum_participants,
      begin_date: begin_date,
      end_date: end_date,
      time_in_hours: time_in_hours,
      seminar_type: seminar_type,
      seminar_manager: user?.id
    }
  });

  console.log(result);

  return result;
}

export default function Create() {
  const { userCount, allSeminars } = useLoaderData<typeof loader>();
  const actionData = useActionData<{ errors?: { [key: string]: string } }>();

  return (
    <div>
      <div className="flex flex-col items-center justify-center h-full">
        <h1 className="font-bold">Seminar einfügen</h1>

        <Form
          action="/create"
          method="POST"
          className="flex flex-col justify-start items-start flex-grow-0 flex-shrink-0 gap-6 p-6 rounded-lg bg-white border border-[#d9d9d9]"
        >
          <div className="flex flex-col justify-start items-start self-stretch flex-grow-0 flex-shrink-0 relative gap-2">
            <label
              htmlFor="title"
              className="self-stretch flex-grow-0 flex-shrink-0 w-[329px] text-base text-left text-[#1e1e1e]"
            >
              Titel
            </label>
            <div className="flex justify-start items-center self-stretch flex-grow-0 flex-shrink-0 relative overflow-hidden px-4 py-3 rounded-lg bg-white border border-[#d9d9d9]">
              <input
                type="text"
                name="title"
                id="title"
                required
                className="flex-grow w-[297px] text-base text-left placeholder:text-[#b3b3b3] outline-none"
                minLength={2}
                maxLength={250}
                />
              {actionData?.errors?.title ? (
                <em className="text-red-500">{actionData?.errors.title}</em>
              ) : null}
            </div>
          </div>
          <div className="flex flex-col justify-start items-start self-stretch flex-grow-0 flex-shrink-0 relative gap-2">
            <label
              htmlFor="description"
              className="self-stretch flex-grow-0 flex-shrink-0 w-[329px] text-base text-left text-[#1e1e1e]"
            >
              Beschreibung
            </label>
            <div className="flex justify-start items-center self-stretch flex-grow-0 flex-shrink-0 relative overflow-hidden px-4 py-3 rounded-lg bg-white border border-[#d9d9d9]">
              <input
                type="text"
                name="description"
                id="description"
                required
                className="flex-grow w-[297px] text-base text-left placeholder:text-[#b3b3b3] outline-none"
                minLength={10}
                maxLength={1000}
              />
              {actionData?.errors?.description ? (
                <em className="text-red-500">{actionData?.errors.description}</em>
              ) : null}
            </div>
          </div>
          <div className="flex flex-col justify-start items-start self-stretch flex-grow-0 flex-shrink-0 relative gap-2">
            <label
              htmlFor="price"
              className="self-stretch flex-grow-0 flex-shrink-0 w-[329px] text-base text-left text-[#1e1e1e]"
            >
              Preis (in €)
            </label>
            <div className="flex justify-start items-center self-stretch flex-grow-0 flex-shrink-0 relative overflow-hidden px-4 py-3 rounded-lg bg-white border border-[#d9d9d9]">
              <input
                type="number"
                name="price"
                id="price"
                required
                className="flex-grow w-[297px] text-base text-left placeholder:text-[#b3b3b3] outline-none"
                min={1}
              />
              {actionData?.errors?.price ? (
                <em className="text-red-500">{actionData?.errors.price}</em>
              ) : null}
            </div>
          </div>
          <div className="flex flex-col justify-start items-start self-stretch flex-grow-0 flex-shrink-0 relative gap-2">
            <label
              htmlFor="maxparticipants"
              className="self-stretch flex-grow-0 flex-shrink-0 w-[329px] text-base text-left text-[#1e1e1e]"
            >
              Maximale Teilnehmeranzahl
            </label>
            <div className="flex justify-start items-center self-stretch flex-grow-0 flex-shrink-0 relative overflow-hidden px-4 py-3 rounded-lg bg-white border border-[#d9d9d9]">
              <input
                type="number"
                name="maximum_participants"
                id="maxparticipants"
                required
                className="flex-grow w-[297px] text-base text-left placeholder:text-[#b3b3b3] outline-none"
                min={1}
              />
              {actionData?.errors?.maximum_participants ? (
                <em className="text-red-500">{actionData?.errors.maximum_participants}</em>
              ) : null}
            </div>
          </div>
          <div className="flex flex-col justify-start items-start self-stretch flex-grow-0 flex-shrink-0 relative gap-2">
            <label
              htmlFor="begindate"
              className="self-stretch flex-grow-0 flex-shrink-0 w-[329px] text-base text-left text-[#1e1e1e]"
            >
              Beginndatum
            </label>
            <div className="flex justify-start items-center self-stretch flex-grow-0 flex-shrink-0 relative overflow-hidden px-4 py-3 rounded-lg bg-white border border-[#d9d9d9]">
              <input
                type="date"
                name="begin_date"
                id="begindate"
                required
                className="flex-grow w-[297px] text-base text-left placeholder:text-[#b3b3b3] outline-none"
              />
              {actionData?.errors?.begin_date ? (
                <em className="text-red-500">{actionData?.errors.begin_date}</em>
              ) : null}
            </div>
          </div>
          <div className="flex flex-col justify-start items-start self-stretch flex-grow-0 flex-shrink-0 relative gap-2">
            <label
              htmlFor="enddate"
              className="self-stretch flex-grow-0 flex-shrink-0 w-[329px] text-base text-left text-[#1e1e1e]"
            >
              Enddatum
            </label>
            <div className="flex justify-start items-center self-stretch flex-grow-0 flex-shrink-0 relative overflow-hidden px-4 py-3 rounded-lg bg-white border border-[#d9d9d9]">
              <input
                type="date"
                name="end_date"
                id="enddate"
                required
                className="flex-grow w-[297px] text-base text-left placeholder:text-[#b3b3b3] outline-none"
              />
              {actionData?.errors?.end_date ? (
                <em className="text-red-500">{actionData?.errors.end_date}</em>
              ) : null}
            </div>
          </div>
          <div className="flex flex-col justify-start items-start self-stretch flex-grow-0 flex-shrink-0 relative gap-2">
            <label
              htmlFor="hoursperday"
              className="self-stretch flex-grow-0 flex-shrink-0 w-[329px] text-base text-left text-[#1e1e1e]"
            >
              Zeitdauer pro Tag (in Stunden)
            </label>
            <div className="flex justify-start items-center self-stretch flex-grow-0 flex-shrink-0 relative overflow-hidden px-4 py-3 rounded-lg bg-white border border-[#d9d9d9]">
              <input
                type="number"
                name="time_in_hours"
                id="hoursperday"
                required
                className="flex-grow w-[297px] text-base text-left placeholder:text-[#b3b3b3] outline-none"
                
              />
              {actionData?.errors?.time_in_hours ? (
                <em className="text-red-500">{actionData?.errors.time_in_hours}</em>
              ) : null}
            </div>
          </div>
          <div className="flex flex-col justify-start items-start self-stretch flex-grow-0 flex-shrink-0 relative gap-2">
            <label
              htmlFor="type"
              className="self-stretch flex-grow-0 flex-shrink-0 w-[329px] text-base text-left text-[#1e1e1e]"
            >
              Seminartyp
            </label>
            <div className="flex justify-start items-center self-stretch flex-grow-0 flex-shrink-0 relative overflow-hidden px-4 py-3 rounded-lg bg-white border border-[#d9d9d9]">
              <select name="seminar_type" id="type">
                <option value="present">Präsenz</option>
                <option value="online">Online</option>
                <option value="hybrid">Hybrid</option>
              </select>
              {actionData?.errors?.seminar_type ? (
                <em className="text-red-500">{actionData?.errors.seminar_type}</em>
              ) : null}
            </div>
          </div>
         {/*  <div className="flex flex-col justify-start items-start self-stretch flex-grow-0 flex-shrink-0 relative gap-2">
            <label
              htmlFor="coursematerial"
              className="self-stretch flex-grow-0 flex-shrink-0 w-[329px] text-base text-left text-[#1e1e1e]"
            >
              Kursmaterial
            </label>
            <div className="flex justify-start items-center self-stretch flex-grow-0 flex-shrink-0 relative overflow-hidden px-4 py-3 rounded-lg bg-white border border-[#d9d9d9]">
              <input
                type="file"
                name="coursematerial"
                id="coursematerial"
                className="flex-grow w-[297px] text-base text-left placeholder:text-[#b3b3b3] outline-none"
              />
            </div>
          </div>  */}

          <div className="flex justify-start items-center flex-grow-0 flex-shrink-0 h-10 gap-4">
            <div className="flex justify-center items-center flex-grow relative overflow-hidden gap-2 p-3 rounded-lg bg-[#1e1e1e] border border-[#2c2c2c]">
              <button
                type="submit"
                className="flex-grow-0 flex-shrink-0 text-base text-left text-neutral-100"
              >
                Abschicken
              </button>
            </div>
          </div>
        </Form>
      </div>

      <br />
    </div>
  );
}
