import type { ActionFunctionArgs } from "@remix-run/node";

export async function action({ context: { res } }: ActionFunctionArgs) {
  res.clearCookie("payload-token");
  return null;
}
