import type { LoaderFunctionArgs } from "@remix-run/node";
import { Link, useLoaderData } from "@remix-run/react";
import { json } from "@remix-run/server-runtime";

export const loader = async ({
  context: { payload, user },
}: LoaderFunctionArgs) => {
  const users = await payload.find({
    collection: "users",
  });
  const seminars = await payload.find({
    collection: "seminars",
  });

  return json(
    { userCount: users.totalDocs, allSeminars: seminars, user },
    { status: 200 }
  );
};

export default function Index() {
  const { userCount, allSeminars, user } = useLoaderData<typeof loader>();
  console.log(userCount);
  //let test = allSeminars.docs[0]?.title;

  return (
    <>
      <h1>Welcome to Kaleidosmetage</h1>
      <h2>Current Users: {user?.email}</h2>
      <ul className="list-disc">
        <li>
          <a target="_blank" href="/admin" rel="noreferrer">
            Admin Interface
          </a>
        </li>
        <li>
          <a target="_blank" href="https://remix.run/docs" rel="noreferrer">
            Remix Docs
          </a>
        </li>
        <li>
          <a
            target="_blank"
            href="https://payloadcms.com/docs"
            rel="noreferrer"
          >
            Payload Docs
          </a>
        </li>
        <li>
          <Link to={"/login"}>Login</Link>
        </li>
      </ul>
    </>
  );
}
