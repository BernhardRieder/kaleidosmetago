import type { ActionFunctionArgs, LoaderFunctionArgs } from "@remix-run/node";
import { json, redirect } from "@remix-run/node";

import { Form, Link, useActionData } from "@remix-run/react";

// import { PayloadRequest } from "payload/types";

export const loader = async ({
  context: { payload, user },
}: LoaderFunctionArgs) => {
  if (user) {
    return redirect("/");
  }

  return json({}, { status: 200 });
};

export async function action({
  request,
  context: { payload, res, req },
}: ActionFunctionArgs) {
  const formData = await request.formData();
  const email = String(formData.get("email"));
  const password = String(formData.get("password"));

  try {
    const result = await payload.login({
      collection: "users", // required
      data: {
        // required
        email,
        password,
      },
      req, // pass a Request object to be provided to all hooks
      res: res, // used to automatically set an HTTP-only auth cookie
    });
    console.log("result", result);
  } catch (e) {
    console.log("error", e);
    return json(
      {
        errors: {
          email: ["Invalid email or password"],
          password: ["Invalid email or password"],
        },
      },
      { status: 400 }
    );
  }

  return null;
}

const FormField = ({
  label,
  id,
  type,
  placeholder,
  error: errors,
}: {
  label: string;
  id: string;
  type: string;
  placeholder: string;
  error?: string[];
}) => (
  <div className="flex flex-col justify-start items-start self-stretch flex-grow-0 flex-shrink-0 relative gap-2">
    <label
      htmlFor={id}
      className="self-stretch flex-grow-0 flex-shrink-0 w-[329px] sm:w-80 text-base text-left text-[#1e1e1e]"
    >
      {label}
    </label>
    <div
      className={`flex justify-start items-center self-stretch flex-grow-0 flex-shrink-0 relative overflow-hidden px-4 py-3 rounded-lg bg-white border ${
        errors ? "border-red-500" : "border-[#d9d9d9]"
      }`}
    >
      <input
        type={type}
        name={id}
        id={id}
        placeholder={placeholder}
        required
        className="flex-grow w-auto text-base text-left placeholder:text-[#b3b3b3] outline-none"
        aria-invalid={errors ? "true" : "false"}
        aria-describedby={errors ? `${id}-error` : undefined}
      />
    </div>
    {errors && (
      <span id={`${id}-error`} className="text-sm text-red-500">
        {errors.join(", ")}
      </span>
    )}
  </div>
);

export default function Login() {
  const actionData = useActionData<typeof action>();

  // Extract field-specific errors from the response
  const getFieldError = (
    fieldName: ["email", "password"][number]
  ): string[] | undefined => {
    if (!actionData?.errors) return undefined;
    if (actionData.errors[fieldName]) {
      return actionData.errors[fieldName];
    }
    return undefined;
  };

  return (
    <div className="flex flex-col items-center justify-center h-full">
      <Form
        action="/login"
        method="post"
        name="login"
        className="flex flex-col justify-start items-start flex-grow-0 flex-shrink-0 gap-6 p-6 sm:rounded-lg sm:bg-white sm:border sm:border-[#d9d9d9] m-8"
      >
        <div className="hidden justify-start items-center flex-grow-0 flex-shrink-0 relative overflow-hidden gap-6 py-1 sm:flex">
          <img
            src="/public/favicon.png"
            className="flex-grow-0 flex-shrink-0 w-20 h-20 object-cover"
            alt="Logo"
          />
          <p className="flex-grow-0 flex-shrink-0 w-56 h-12 text-2xl font-extralight text-left text-[#6a6a6a]">
            KALEIDOSMETAGO
          </p>
        </div>
        <FormField
          label="Email"
          id="email"
          type="email"
          placeholder="Email"
          error={getFieldError("email")}
        />

        <FormField
          label="Passwort"
          id="password"
          type="password"
          placeholder="Passwort"
          error={getFieldError("password")}
        />
        <div className="flex justify-start items-center self-stretch flex-grow-0 flex-shrink-0 h-10 gap-4">
          <button
            type="submit"
            className="flex justify-center items-center flex-grow relative overflow-hidden gap-2 p-3 rounded-lg bg-[#1e1e1e] border border-[#2c2c2c]"
          >
            <span className="flex-grow-0 flex-shrink-0 text-base text-left text-neutral-100">
              Anmelden
            </span>
          </button>
        </div>
        <div className="flex justify-between items-start self-stretch flex-grow-0 flex-shrink-0 relative overflow-hidden">
          <Link
            to={"/forgot-password"}
            className="flex-grow-0 flex-shrink-0 text-base text-left text-[#1e1e1e]"
          >
            Passwort vergessen?
          </Link>
          <Link
            to="/register"
            className="flex-grow-0 flex-shrink-0 text-base text-left text-black"
          >
            Registrieren
          </Link>
        </div>
      </Form>
    </div>
  );
}
