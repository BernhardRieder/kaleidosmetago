import type { LoaderFunctionArgs, MetaFunction } from "@remix-run/node";
import {
  json,
  Links,
  Meta,
  Outlet,
  Scripts,
  ScrollRestoration,
  useLoaderData,
} from "@remix-run/react";

//css should be imported as an side effect for Vite
import Footer from "./components/footer/footer";
import Header from "./components/header/header";
import "./tailwind.css";

export const meta: MetaFunction = () => [
  { title: "Kaleidosmetago" },
  { charSet: "utf-8" },
  { name: "viewport", content: "width=device-width, initial-scale=1" },
];

export const loader = async ({
  context: { payload, user },
}: LoaderFunctionArgs) => {
  return json({ user }, { status: 200 });
};

export default function App() {
  const { user } = useLoaderData<typeof loader>();
  return (
    <html lang="en">
      <head>
        <Meta />
        <Links />
      </head>
      <body>
        <div
          style={{ fontFamily: "system-ui, sans-serif, ", lineHeight: "1.4" }}
          className="min-h-screen flex flex-col justify-between"
        >
          <Header user={user}></Header>
          <Outlet />
          <Footer></Footer>
        </div>
        <ScrollRestoration />
        <Scripts />
      </body>
    </html>
  );
}
