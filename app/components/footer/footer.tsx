import { Link } from "@remix-run/react";
import "./styles.css";

export default function Footer() {
  return (
    <div className="ml-auto mr-auto w-full md:flex items-center content-between justify-between footerTextFont footerBorder bottom-0">
      <div className="flex items-center content-center justify-center gap-2 ">
        <Link to="/impressum">Impressum</Link>
        <Link to="/datenschutz">Datenschutz</Link>
        <Link to="/agb">AGB</Link>
      </div>
      <div className="hidden md:flex flex-col">
        <p className="font-bold ml-auto">Kaleidosmetago</p>
        <p className="">Akademie für Nachhaltig Wirtschaften und Gut Leben</p>
      </div>
    </div>
  );
}
