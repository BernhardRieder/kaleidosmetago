import { Form, Link } from "@remix-run/react";
import type { User } from "payload/generated-types";
import { useState } from "react";
import "./styles.css";

export default function Header({ user }: { user?: User }) {
  const loggedIn = user?.email !== undefined;
  const [burgerMenuOpen, setBurgerMenuOpen] = useState(false);

  return (
    <>
      <div className="flex items-center content-between justify-between headerBorder w-screen ">
        <div className="flex items-center content-start justify-start">
          <Link className="h-full w-[50px]" to="/">
            <img
              className="pl-1 size-full"
              src="../../public/favicon.png"
              alt="Logo"
            ></img>
          </Link>
          <div>
            <Link to="/" className="text-xl font-thin logoText pl-2 ">
              KALEIDOSMETAGO
            </Link>
          </div>
        </div>
        <div className="hidden md:flex items-center content-evenly justify-evenly gap-3 pr-6 text-[1rem] headerTextFont">
          <div>
            <Link
              to=""
              className="no-underline  text-black hover:bg-[#F5F5F5] rounded-[4px]"
            >
              Seminare
            </Link>
          </div>
          <div>
            <Link
              to=""
              className="no-underline text-black hover:bg-[#F5F5F5] rounded-[4px]"
            >
              Betreuer
            </Link>
          </div>
          <div>
            <Link
              to=""
              className="no-underline text-black hover:bg-[#F5F5F5] rounded-[4px]"
            >
              Kaleidosmetago
            </Link>
          </div>
          <div>
            <Link
              to=""
              className="no-underline text-black hover:bg-[#F5F5F5] rounded-[4px]"
            >
              Kontakt
            </Link>
          </div>
          <div>
            <Link
              className={
                loggedIn
                  ? "hidden"
                  : "loginButtonFont headerButtonScale flex flex-row justify-center items-center px-[24px] py-[12px] gap-[12px] w-[124px] h-[40px] bg-[#00B7E4] rounded-[16px] order-1 flex-grow-0 hover:bg-[#6cace4]"
              }
              to={"/login"}
            >
              Anmelden
            </Link>
            <Form action="/logout" method="post" navigate={false}>
              <button
                className={
                  loggedIn
                    ? "headerButtonScale flex flex-row justify-center items-center px-[24px] py-[12px] gap-[12px] w-[124px] h-[40px] bg-[#00B7E4] rounded-[16px] order-1 flex-grow-0 hover:bg-[#6cace4]"
                    : "hidden"
                }
                type="submit"
              >
                Logout
              </button>
            </Form>
          </div>
          <div>
            <Link
              className={
                loggedIn
                  ? "hidden"
                  : "registerButtonFont headerButtonScale flex flex-row justify-center items-center px-[24px] py-[12px] gap-[12px] w-[124px] h-[40px] bg-[#00B7E4] rounded-[16px] order-1 flex-grow-0 hover:bg-[#6cace4]"
              }
              to={"/register"}
            >
              Registrieren
            </Link>
            <button
              className={
                loggedIn
                  ? "flex flex-row justify-center items-center w-[60px] h-[53px] bg-[#D9D9D9] rounded-[20px] flex-none order-2 flex-grow-0"
                  : "hidden"
              }
            >
              <svg
                width="25"
                height="25"
                viewBox="0 0 25 25"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  d="M20.7452 22.1L20.7456 18.5003C20.7457 16.512 19.1066 14.9 17.0845 14.9H7.32256C5.3008 14.9 3.66177 16.5116 3.66154 18.4996L3.66113 22.1M15.8645 6.50002C15.8645 8.48825 14.2254 10.1 12.2035 10.1C10.1816 10.1 8.54249 8.48825 8.54249 6.50002C8.54249 4.5118 10.1816 2.90002 12.2035 2.90002C14.2254 2.90002 15.8645 4.5118 15.8645 6.50002Z"
                  stroke="black"
                  strokeWidth="2"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                />
              </svg>
            </button>
          </div>
        </div>
        <div className={!burgerMenuOpen ? "pr-3 md:hidden" : "hidden"}>
          <button
            onClick={() => {
              if (!burgerMenuOpen) {
                setBurgerMenuOpen(true);
              }
            }}
          >
            <svg
              width="29"
              height="22"
              viewBox="0 0 29 22"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                d="M2.875 11H26.125M2.875 2.75H26.125M2.875 19.25H26.125"
                stroke="#1E1E1E"
                strokeWidth="4"
                strokeLinecap="round"
                strokeLinejoin="round"
              />
            </svg>
          </button>
        </div>
        <div className={burgerMenuOpen ? "pr-3 md:hidden" : "hidden"}>
          <button
            onClick={() => {
              if (burgerMenuOpen) {
                setBurgerMenuOpen(false);
              }
            }}
          >
            <svg
              width="31"
              height="34"
              viewBox="0 0 31 34"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                d="M23.25 8.75L7.75 25.25M7.75 8.75L23.25 25.25"
                stroke="#1E1E1E"
                strokeWidth="4"
                strokeLinecap="round"
                strokeLinejoin="round"
              />
            </svg>{" "}
          </button>
        </div>
      </div>
      <div
        className={
          burgerMenuOpen
            ? "flex flex-col items-center content-evenly justify-evenly bg-[#EDE9E9] gap-4 pt-[4px] headerTextFont md:hidden"
            : "hidden"
        }
      >
        <div className="bg-[#F5F5F5] rounded-[6px] p-[2px]">
          <Link to="" className="no-underline text-black">
            Seminare
          </Link>
        </div>

        <div className="bg-[#F5F5F5] rounded-[6px] p-[2px]">
          <Link to="" className="no-underline text-black">
            Betreuer
          </Link>
        </div>

        <div className="bg-[#F5F5F5] rounded-[6px] p-[2px]">
          <Link to="" className="no-underline text-black">
            Kaleidosmetago
          </Link>
        </div>

        <div className="bg-[#F5F5F5] rounded-[6px] p-[2px]">
          <Link to="" className="no-underline text-black">
            Kontakt
          </Link>
        </div>
        <div className={loggedIn ? "hidden" : ""}>
          <Link
            className="headerButtonScale flex flex-row justify-center items-center px-[24px] py-[12px] gap-[12px] w-[124px] h-[40px] bg-[#00B7E4] rounded-[16px] order-1 flex-grow-0 hover:bg-[#6cace4]"
            to={"/login"}
          >
            Anmelden
          </Link>
        </div>
        <div className={loggedIn ? "hidden" : ""}>
          <Link
            className="headerButtonScale flex flex-row justify-center items-center px-[24px] py-[12px] gap-[12px] w-[124px] h-[40px] bg-[#00B7E4] rounded-[16px] order-1 flex-grow-0 hover:bg-[#6cace4]"
            to={"/register"}
          >
            Registrieren
          </Link>
        </div>
        <div className={loggedIn ? "" : "hidden"}>
          <Form action="/logout" method="post" navigate={false}>
            <button
              type="submit"
              className="headerButtonScale flex flex-row justify-center items-center px-[24px] py-[12px] gap-[12px] w-[124px] h-[40px] bg-[#00B7E4] rounded-[16px] order-1 flex-grow-0 hover:bg-[#6cace4]"
            >
              Logout
            </button>
          </Form>
        </div>
        <div className={loggedIn ? "" : "hidden"}>
          <button className="flex flex-row justify-center items-center w-[60px] h-[53px] bg-[#D9D9D9] rounded-[20px] flex-none order-2 flex-grow-0">
            <svg
              width="25"
              height="25"
              viewBox="0 0 25 25"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                d="M20.7452 22.1L20.7456 18.5003C20.7457 16.512 19.1066 14.9 17.0845 14.9H7.32256C5.3008 14.9 3.66177 16.5116 3.66154 18.4996L3.66113 22.1M15.8645 6.50002C15.8645 8.48825 14.2254 10.1 12.2035 10.1C10.1816 10.1 8.54249 8.48825 8.54249 6.50002C8.54249 4.5118 10.1816 2.90002 12.2035 2.90002C14.2254 2.90002 15.8645 4.5118 15.8645 6.50002Z"
                stroke="black"
                strokeWidth="2"
                strokeLinecap="round"
                strokeLinejoin="round"
              />
            </svg>
          </button>
        </div>
        <br />
      </div>
    </>
  );
}
